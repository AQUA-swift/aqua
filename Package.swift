// swift-tools-version:4.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "AQUA",
    products: [
		.library(name: "Extensions", targets: ["Extensions"]),
		.library(name: "Operations", targets: ["Operations"]),
    ],
    dependencies: [],
    targets: [
		.target(name: "Extensions", dependencies: []),
		.target(name: "Operations", dependencies: ["Extensions"]),
		.testTarget(name: "ExtensionsTests", dependencies: ["Extensions"]),
		.testTarget(name: "OperationsTests", dependencies: ["Operations"]),
    ]
)
