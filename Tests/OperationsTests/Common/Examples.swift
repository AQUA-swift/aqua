//
//  Examples.swift
//  OperationsTests
//
//  Created by NShtain on 27/12/2018.
//

import Foundation
import Operations

struct Examples {
	private init() {}
}

extension Examples {
	class IncrementOperation: COperation, InputOperation, OutputOperation {
		var input: Pending<Int> = .pending
		var output: Pending<OperationResult<Int>> = .pending

		init(value: Int) {
			super.init()
			set(input: value)
		}

		override init() {
			super.init()
		}

		override func execute() {
			guard case .ready(let number) = input
				else {
					cancel()
					return
			}
			finish(withResult: .success(number + 1))
		}
	}
}

extension Examples {
	class SendErrorOperation: COperation, OutputOperation {
		var output: Pending<OperationResult<String>> = .pending
		let error: Error
		init(error: Error) {
			self.error = error
			super.init()
			self.ready()
		}

		override func execute() {
			finish(withResult: .failure(error))
		}
	}
}

extension Examples {
	class PrintResultOperation<T: CustomStringConvertible>: COperation, InputOperation {
		var input: Pending<T> = .pending
		override init() {
			super.init()
		}

		override func execute() {
			defer { finish() }
			switch input {
			case .pending: return // TODO: Make error?
			case .ready(let value):
				print(value)
			}
		}
	}
}

extension Examples {
	class BlockOperation: COperation {
		typealias Block = () -> Void

		let block: Block

		init(result: @escaping Block) {
			self.block = result
			super.init()
			ready()
		}

		override func execute() {
			defer { finish() }
			block()
		}
	}
}

extension Examples {
	class CallbackOperation<T>: COperation, InputOperation {
		typealias CallbackBlock = (T) -> Void

		var input: Pending<T> = .pending
		let callbackBlock: CallbackBlock

		init(result: @escaping CallbackBlock) {
			self.callbackBlock = result
			super.init()
		}

		override func execute() {
			defer { finish() }

			switch input {
			case .pending:
				fatalError("WTF?")
			case .ready(let value):
				callbackBlock(value)
			}
		}
	}
}

extension Examples {
	class MapOperation<T: Decodable>: COperation, InputOperation, OutputOperation {
		var input: Pending<Data> = .pending
		var output: Pending<OperationResult<T>> = .pending

		override func execute() {
			guard case .ready(let data) = input
				else { fatalError() }

			let decoder = JSONDecoder()
			do {
				let result = try decoder.decode(T.self, from: data)
				finish(withResult: .success(result))
			} catch let catched {
				finish(withResult: .failure(catched))
			}
		}
	}
}

extension Examples {
	class PrintTimeOperation: COperation {
		override init() {
			super.init()
			ready()
		}

		override func execute() {
			defer { finish() }
			let date = Date()
			print(date.toString(.full))
		}
	}
}
