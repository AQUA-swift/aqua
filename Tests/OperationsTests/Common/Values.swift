//
//  Values.swift
//  Operations
//
//  Created by NShtain on 29/01/2019.
//

import Foundation

struct Values {
	private init() {}
	static let defaultWaitInterval: Double = 1.5
	static let longWaitInterval: Double = 5
	static let minuteInterval: Double = 60
}
