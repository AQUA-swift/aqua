//
//  Photo.swift
//  OperationsTests
//
//  Created by NShtain on 23/01/2019.
//

import Foundation

struct Photo: Codable {
	let uid: Int
	let album: Int
	let title: String

	let url: URL
	let thumbnailUrl: URL

	enum CodingKeys: String, CodingKey {
		case uid = "id"
		case album = "albumId"
		case title, url, thumbnailUrl
	}
}
