//
//  Post.swift
//  OperationsTests
//
//  Created by NShtain on 22/01/2019.
//

import Foundation

struct Post: Codable {
	let userId: Int
	let uid: Int
	let title: String
	let body: String

	enum CodingKeys: String, CodingKey {
		case userId, title, body
		case uid = "id"
	}
}

struct CreatePost: Encodable {
	let userId: Int
	let title: String
	let body: String
}

struct UpdatePost: Encodable {
	let uid: Int
	let title: String
	let body: String

	enum CodingKeys: String, CodingKey {
		case title, body
	}
}
