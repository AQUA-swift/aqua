//
//  Requests.swift
//  OperationsTests
//
//  Created by NShtain on 22/01/2019.
//

import Foundation
import Operations

private struct Route: Operations.Route {
	static var host: URL = URL(string: "https://jsonplaceholder.typicode.com")!
	static var apiPath: String? = nil

	let path: String
	let method: RequestMethod

	init(path: String, method: RequestMethod) {
		self.path = path
		self.method = method
	}
}

struct Request<R: Decodable>: Operations.Request {
	typealias Out = R

	var route: Operations.Route
	let headers: [String : String]?
	let data: RequestData

	fileprivate init(path: String, method: RequestMethod = .get, headers: [String : String]? = nil, data: RequestData = .void, result: R.Type) {
		self.route = Route(path: path, method: method)
		self.headers = headers
		self.data = data
	}
}

private extension Request where R == NoReply {
	init(path: String, method: RequestMethod = .get, headers: [String : String]? = nil, data: RequestData = .void, result: R.Type = NoReply.self) {
		self.route = Route(path: path, method: method)
		self.headers = headers
		self.data = data
	}
}

struct Requests {
	private init() {}
}

extension Requests {
	static func posts() -> Request<[Post]> {
		return Request(path: "/posts", result: [Post].self)
	}
	static func post(with uid: Int) -> Request<Post> {
		return Request(path: "/posts/\(uid)", result: Post.self)
	}
	static func comments(post: Int) -> Request<NoReply> {
		return Request(
			path: "/comments",
			data: .dictionary(["postId": post]))
	}
	static func create(post: CreatePost) -> Request<NoReply> {
		return Request(path: "/posts", method: .post, data: .encodable(post))
	}
	static func update(post: UpdatePost) -> Request<NoReply> {
		return Request(path: "/posts/\(post.uid)", method: .patch, data: .encodable(post))
	}

	static func photos() -> Request<NoReply> {
		return Request(path: "/photos")
	}

	/*
	Routes
	All HTTP methods are supported.

	+ GET	/posts
	+ GET	/posts/1
	- GET	/posts/1/comments
	+ GET	/comments?postId=1
	- GET	/posts?userId=1
	+ POST	/posts
	- PUT	/posts/1
	+ PATCH	/posts/1
	- DELETE	/posts/1
	*/
}
