//
//  Errors.swift
//  OperationsTests
//
//  Created by NShtain on 27/12/2018.
//

import Foundation

enum Errors: Error {
	case empty
	case inputNotFound
}
