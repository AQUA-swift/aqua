//
//  LoadImageOperationTests.swift
//  OperationsTests
//
//  Created by NShtain on 23/01/2019.
//

import XCTest
@testable import Operations

#if canImport(UIKit)
import UIKit
#elseif canImport(AppKit)
import AppKit
#endif

#if canImport(UIKit) || canImport(AppKit)

final class LoadImageOperationTests: XCTestCase {
	private let queueName = "LoadImageOperationTestsQueue"
	var queue: OperationQueue {
		return OperationQueue(name: queueName)
	}

	func testLoadImage() {
		let expectation = self.expectation(description: "LoadImage")
		
		let endpoint = Requests.photos()

		let network = NetworkOperation()
		network.set(input: endpoint)

		let map = Examples.MapOperation<[Photo]>()
		let image = LoadImageOperation()

		var result: Image!
		let callback = Examples.CallbackOperation<Image> { (image) in
			result = image
			expectation.fulfill()
		}

		network.attach(to: map, select: { $0.data })
		map.attach(to: image, select: { $0.last!.url })
		image.attach(to: callback)

		queue.addOperations([network, image, map, callback], waitUntilFinished: false)
		waitForExpectations(timeout: Values.longWaitInterval, handler: nil)
		XCTAssert(result != nil, "Didn't load image")
	}
}

#endif
