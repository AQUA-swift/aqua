//
//  NetworkOperationTests.swift
//  OperationsTests
//
//  Created by NShtain on 22/01/2019.
//

import Foundation

import XCTest
@testable import Operations

final class NetworkOperationTests: XCTestCase {
	private let queueName = "NetworkOperationTestsQueue"
	private var queue: OperationQueue {
		return OperationQueue(name: queueName)
	}

	func testGetRequest() {
		let expectation = self.expectation(description: "GetRequest")

		let network = NetworkOperation()
		let endpoint = Requests.posts()
		network.set(input: endpoint)

		var result: Data!
		let callback = Examples.CallbackOperation<ResponseData> { (response) in
			result = response.data
			if let object = try? JSONSerialization.jsonObject(with: response.data, options: []) {
				print(object)
			}
			expectation.fulfill()
		}

		network.attach(to: callback)

		queue.addOperations([network, callback], waitUntilFinished: false)
		waitForExpectations(timeout: Values.defaultWaitInterval, handler: nil)
		XCTAssert(result != nil, "Failed request")
	}

	func testGetRequestWithParametersInUrl() {
		let expectation = self.expectation(description: "GetRequest")

		let network = NetworkOperation()
		let endpoint = Requests.post(with: 1)
		network.set(input: endpoint)

		var result: Data!
		let callback = Examples.CallbackOperation<ResponseData> { (response) in
			result = response.data
			if let object = try? JSONSerialization.jsonObject(with: response.data, options: []) {
				print(object)
			}
			expectation.fulfill()
		}

		network.attach(to: callback)

		queue.addOperations([network, callback], waitUntilFinished: false)
		waitForExpectations(timeout: Values.defaultWaitInterval, handler: nil)
		XCTAssert(result != nil, "Failed request")
	}

	func testGetRequestWithParametrs() {
		let expectation = self.expectation(description: "GetRequest")

		let network = NetworkOperation()
		let endpoint = Requests.comments(post: 1)
		network.set(input: endpoint)

		var result: Data!
		let callback = Examples.CallbackOperation<ResponseData> { (response) in
			result = response.data
			if let object = try? JSONSerialization.jsonObject(with: response.data, options: []) {
				print(object)
			}
			expectation.fulfill()
		}

		network.attach(to: callback)

		queue.addOperations([network, callback], waitUntilFinished: false)
		waitForExpectations(timeout: Values.defaultWaitInterval, handler: nil)
		XCTAssert(result != nil, "Failed request")
	}

	func testPostRequest() {
		let expectation = self.expectation(description: "GetRequest")
		let post = CreatePost(userId: 1, title: "Test", body: "Body text in test post")

		let network = NetworkOperation()
		let endpoint = Requests.create(post: post)

		network.set(input: endpoint)

		var result: Data!
		let callback = Examples.CallbackOperation<ResponseData> { (response) in
			result = response.data
			if let object = try? JSONSerialization.jsonObject(with: response.data, options: []) {
				print(object)
			}
			expectation.fulfill()
		}

		network.attach(to: callback)

		queue.addOperations([network, callback], waitUntilFinished: false)
		waitForExpectations(timeout: Values.defaultWaitInterval, handler: nil)
		XCTAssert(result != nil, "Failed request")
	}

	func testPatchRequest() {
		let expectation = self.expectation(description: "GetRequest")
		let post = UpdatePost(
			uid: 1,
			title: "Change Title",
			body: "text text text text text text text text text"
		)

		let network = NetworkOperation()
		let endpoint = Requests.update(post: post)

		network.set(input: endpoint)

		var result: Data!
		let callback = Examples.CallbackOperation<ResponseData> { (response) in
			result = response.data
			if let object = try? JSONSerialization.jsonObject(with: response.data, options: []) {
				print(object)
			}
			expectation.fulfill()
		}

		network.attach(to: callback)

		queue.addOperations([network, callback], waitUntilFinished: false)
		waitForExpectations(timeout: Values.defaultWaitInterval, handler: nil)
		XCTAssert(result != nil, "Failed request")
	}
}


