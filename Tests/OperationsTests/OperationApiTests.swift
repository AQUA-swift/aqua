//
//  OperationApiTests.swift
//  OperationsTests
//
//  Created by NShtain on 27/12/2018.
//

import XCTest
@testable import Operations

final class OperationApiTests: XCTestCase {
	private let queueName = "OperationApiTestsQueue"
	var queue: OperationQueue {
		return OperationQueue(name: queueName)
	}

	func testInject() {
		let expectation = self.expectation(description: "Inject")

		let increment = Examples.IncrementOperation(value: 5)

		var result: Int!
		let callback = Examples.CallbackOperation<Int> { (value) in
			result = value
			expectation.fulfill()
		}

		increment.attach(to: callback)
		queue.addOperations([increment, callback], waitUntilFinished: false)

		waitForExpectations(timeout: Values.defaultWaitInterval, handler: nil)
		XCTAssert(result == 6, "Inject increment operation failed: \(String(describing: result)) != 6")
	}

	func testAutoCancel() {
		let expectation = self.expectation(description: "AutoCancel")
		expectation.isInverted = true

		let sendError = Examples.SendErrorOperation(error: Errors.empty)

		var result: String!
		let callback = Examples.CallbackOperation<String> { (value) in
			result = value
			expectation.fulfill()
		}

		sendError.attach(to: callback)
		queue.addOperations([sendError, callback], waitUntilFinished: false)

		waitForExpectations(timeout: Values.defaultWaitInterval, handler: nil)
		XCTAssert(callback.isCancelled, "with: \(String(describing: result))")
	}
}
