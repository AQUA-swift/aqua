//
//  OperationsAdapterTests.swift
//  OperationsTests
//
//  Created by NShtain on 01/02/2019.
//

import XCTest
@testable import Operations
import Extensions

final class OperationsAdapterTests: XCTestCase {
	private let queueName = "OperationsAdapterTestsQueue"
	var queue: OperationQueue {
		return OperationQueue(name: queueName)
	}

	func testCancelFewInLineOperations() {
		let expectation = self.expectation(description: "CancelFewInLineOperations")

		let start = CreateIntOrErrorOperation(success: false)
		let second = Examples.IncrementOperation()

		var result: OperationResult<Int>!

		let callback = Examples.CallbackOperation<Int> { (number) in
			result = .success(number)
			expectation.fulfill()
		}

		let errorClosure = Examples.CallbackOperation<Error> { (error) in
			result = .failure(error)
			expectation.fulfill()
		}

		start.attach(to: second)
		second.attach(to: callback)

		start.resend(errorToInput: errorClosure)
		second.resend(errorToInput: errorClosure)

		errorClosure.add(dependencies: start, second, callback)

		let operations = [errorClosure, start, second, callback]
		queue.addOperations(operations, waitUntilFinished: false)

		waitForExpectations(timeout: Values.defaultWaitInterval, handler: nil)

		let executing = operations.map { $0.isFinished || $0.isCancelled }.contains(false)
		XCTAssert(result != nil, "Lost result")
		XCTAssert(!executing, "Failed cascade cancel operation")
	}

	func testLongLineDependenciesCancel() {
		func newPrintOperation() -> COperation {
			return Examples.PrintTimeOperation()
		}
		func makeChild(count: Int) -> [COperation] {
			let items = Array(repeating: "item", count: count)
			return items.map { _ in newPrintOperation() }
		}

		let expectation = self.expectation(description: "NetworkGroup")

		let childs: [COperation] = makeChild(count: 40)

		var preview: COperation?
		for child in childs {
			if let preview = preview {
				child.add(dependency: preview)
				preview.add(relation: .cancel, for: child)
			}
			preview = child
		}

		let block = Examples.BlockOperation {
			expectation.fulfill()
		}

		block.add(dependency: childs.last!)

		queue.addOperation(block)
		queue.addOperations(childs, waitUntilFinished: false)

		childs[20].cancel()

		waitForExpectations(timeout: Values.longWaitInterval, handler: nil)

		let statuses = childs.compactMap { $0.isCancelled }
		var canceled = 0
		for status in statuses {
			canceled += status ? 1 : 0
		}

		XCTAssert(canceled == 20, "Cancel \(canceled) operation")
	}
}

fileprivate class CreateIntOrErrorOperation: COperation, OutputOperation {
	var output: Pending<OperationResult<Int>> = .pending

	let success: Bool
	init(success: Bool) {
		self.success = success
		super.init()
		ready()
	}

	override func execute() {
		if success {
			finish(withResult: .success(1))
		} else {
			let error = ApplicationError(message: "Text error")
			finish(withResult: .failure(error))
		}
	}
}
