//
//  OperationGroupsTests.swift
//  OperationsTests
//
//  Created by NShtain on 16/01/2019.
//

import XCTest
@testable import Operations

final class OperationGroupsTests: XCTestCase {
	private let queueName = "OperationApiTestsQueue"
	private var queue: OperationQueue {
		return OperationQueue(name: queueName)
	}

	func testNetworkGroup() {
		let expectation = self.expectation(description: "NetworkGroup")

		let network = NetworkGroup(of: Request<[Post]>.self)
		network.set(input: Requests.posts())

		var result: [Post]!
		let callback = Examples.CallbackOperation<[Post]> { (data) in
			result = data
			expectation.fulfill()
		}

		network.attach(to: callback)

		queue.addOperations([network, callback], waitUntilFinished: false)
		waitForExpectations(timeout: Values.defaultWaitInterval, handler: nil)
		XCTAssert(result != nil, "Failed request")
	}

	func testFlatGroup() {
		func newPrintOperation() -> COperation {
			return Examples.PrintTimeOperation()
		}
		func makeChild(count: Int) -> [COperation] {
			let items = Array(repeating: "item", count: count)
			return items.map { _ in newPrintOperation() }
		}

		let expectation = self.expectation(description: "NetworkGroup")

		let childs: [COperation] = makeChild(count: 40)
		let group = FlatGroup(operations: childs)

		var result: Bool = false
		let block = Examples.BlockOperation {
			result = true
			expectation.fulfill()
		}

		block.add(dependency: group)
		group.ready()

		queue.addOperations([group, block], waitUntilFinished: false)
		waitForExpectations(timeout: Values.longWaitInterval, handler: nil)
		XCTAssert(result, "Failed flat group")
	}
}
