//
//  FlatGroup.swift
//  Operations
//
//  Created by NShtain on 28/01/2019.
//

import Foundation

class FlatGroup: CGroup {
	private let queueName = "NetworkOperationQueue_" + UUID().uuidString

	init(operations: [COperation]) {
		super.init(queue: queueName, childs: operations)
	}

	convenience init(operations: COperation...) {
		self.init(operations: operations)
	}

	@discardableResult
	func add(operation: COperation) -> Bool {
		if status == .pending {
			childs.append(operation)
			return true
		} else {
			return false
		}
	}
}
