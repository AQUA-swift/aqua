//
//  DecodeOperation.swift
//  Operations
//
//  Created by NShtain on 25/01/2019.
//

import Foundation

public class DecodeOperation<T: Decodable>: COperation, InputOperation, OutputOperation {
	public var input: Pending<Data> = .pending
	public var output: Pending<OperationResult<T>> = .pending

	public init(of type: T.Type) {
		super.init()
	}

	public override func execute() {
		guard case .ready(let data) = input
			else { fatalError() }

		let decoder = JSONDecoder()
		do {
			let result = try decoder.decode(T.self, from: data)
			finish(withResult: .success(result))
		} catch let catched {
			finish(withResult: .failure(catched))
		}
	}
}
