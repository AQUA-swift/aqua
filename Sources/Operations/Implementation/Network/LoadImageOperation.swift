//
//  LoadImageOperation.swift
//  Operations
//
//  Created by NShtain on 25/01/2019.
//

#if canImport(UIKit)
import UIKit
public typealias Image = UIImage
#elseif canImport(AppKit)
import AppKit
public typealias Image = NSImage
#endif

#if canImport(UIKit) || canImport(AppKit)

public final class LoadImageOperation: COperation, InputOperation, OutputOperation {
	public var input: Pending<URL> = .pending
	public var output: Pending<OperationResult<Image>> = .pending

	public override func execute() {
		guard case .ready(let url) = input
			else { fatalError() }

		let configuration = URLSessionConfiguration.ephemeral
		let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)

		print("[LoadImageOperation] start request: \(url)")

		let task = session.dataTask(with: url) { [weak self] (data, response, error) in
			if let statusCode = response?.status?.toString() {
				print("[LoadImageOperation] response.statusCode: \(statusCode)")
			}
			if let data = data, let image = Image(data: data) {
				self?.finish(withResult: .success(image))
			} else if let error = error {
				self?.finish(withResult: .failure(error))
			} else {
				fatalError()
			}
		}
		task.resume()
	}
}

#endif
