//
//  PerformStatusOperation.swift
//  Operations
//
//  Created by NShtain on 31/01/2019.
//

import Foundation
import Extensions

public class PerformStatusOperation: COperation, InputOperation, OutputOperation {
	public var input: Pending<ResponseData> = .pending
	public var output: Pending<OperationResult<Data>> = .pending

	public override init() {
		super.init()
	}

	public override func execute() {
		guard case .ready(let response) = input
			else { fatalError() }
		if response.statusCode >= 200 && response.statusCode < 300 {
			finish(withResult: .success(response.data))
		} else if response.statusCode >= 400 || response.statusCode < 600 {
			let error = NetworkError(statusCode: response.statusCode, data: response.data)
			finish(withResult: .failure(error))
		} else {
			let error = ApplicationError(message: "Unsupported status code")
			finish(withResult: .failure(error))
		}
	}
}
