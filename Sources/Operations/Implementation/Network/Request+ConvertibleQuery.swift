//
//  Request+ConvertibleQuery.swift
//  Operations
//
//  Created by NShtain on 25/01/2019.
//

import Foundation

extension Request {
	public func toRequest() -> URLRequest {
		var request: URLRequest!

		switch route.method {
		case .get:
			var urlComponents = URLComponents(string: url.absoluteString)!
			urlComponents.queryItems = queryItems()
			request = URLRequest(url: urlComponents.url!)
		case .post, .patch, .put:
			request = URLRequest(url: url)
			request.httpBody = queryBody()
		case .delete:
			request = URLRequest(url: url)
		}
		request.httpMethod = route.method.name
		request.allHTTPHeaderFields = queryHeaders()
		return request
	}
}

extension Request {
	private var url: URL {
		let staticRoute = type(of: route)

		var url = staticRoute.host
		if let api = staticRoute.apiPath {
			url = url.appendingPathComponent(api)
		}
		url = url.appendingPathComponent(route.path)

		return url
	}

	private func queryItems() -> [URLQueryItem]? {
		func from(dictionary: [String:Any]) -> [URLQueryItem]? {
			let components = self.components(dictionary)
			let items = components.map { URLQueryItem(name: $0.0, value: $0.1) }
			return items.isEmpty ? nil : items
		}

		switch self.data {
		case .dictionary(let dictionary):
			return from(dictionary: dictionary)
		case .encodable(let item):
			guard let dictionary = item.toDictionary()
				else { return nil }
			return from(dictionary: dictionary)
		case .void:
			return nil
		}
	}

	private func queryHeaders() -> [String: String] {
		var headers = self.headers ?? [:]
		switch route.method {
		case .post, .put, .patch:
			headers["Content-Type"] = "Application/json"
		default: break
		}
		return headers
	}

	private func queryBody() -> Data? {
		switch data {
		case .dictionary(let dictionary):
			return try? JSONSerialization.data(withJSONObject: dictionary, options: [])
		case .encodable(let item):
			return item.toJsonData()
		case .void:
			return nil
		}
	}

	internal func createRequest() -> URLRequest {
		var request: URLRequest!

		switch route.method {
		case .get:
			var urlComponents = URLComponents(string: url.absoluteString)!
			urlComponents.queryItems = queryItems()
			request = URLRequest(url: urlComponents.url!)
		case .post, .patch, .put:
			request = URLRequest(url: url)
			request.httpBody = queryBody()
		case .delete:
			request = URLRequest(url: url)
		}
		request.httpMethod = route.method.name
		request.allHTTPHeaderFields = queryHeaders()
		return request
	}
}

extension Request {
	private func components(_ parameters: [String: Any]) -> [(String, String)] {
		var components: [(String, String)] = []

		for key in parameters.keys.sorted(by: <) {
			let value = parameters[key]!
			components += queryComponents(fromKey: key, value: value)
		}
		return components
	}

	private func queryComponents(fromKey key: String, value: Any) -> [(String, String)] {
		var components: [(String, String)] = []

		switch value {
		case let dictionary as [String: Any]:
			for (nestedKey, value) in dictionary {
				components += queryComponents(fromKey: "\(key)[\(nestedKey)]", value: value)
			}
		case let array as [Any]:
			for value in array {
				components += queryComponents(fromKey: key.escaped(), value: value)
			}
		case let value as NSNumber:
			components.append((key.escaped(), "\(value)".escaped()))
		case let bool as Bool:
			components.append((key.escaped(), "\(bool)".escaped()))
		default:
			components.append((key.escaped(), "\(value)".escaped()))
		}

		return components
	}
}
