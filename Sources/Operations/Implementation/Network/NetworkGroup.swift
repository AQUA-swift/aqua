//
//  NetworkGroup.swift
//  Operations
//
//  Created by NShtain on 25/01/2019.
//

import Foundation

open class NetworkGroup<R: Request>: CGroup, InputOperation, OutputOperation {
	public var input: Pending<R> = .pending
	public var output: Pending<OperationResult<R.Out>> = .pending

	private let queueName = "NetworkOperationQueue_" + UUID().uuidString

	public init(of type: R.Type) {
		let network = NetworkOperation()
		let status = PerformStatusOperation()
		let decode = DecodeOperation(of: R.Out.self)

		network.attach(to: status)
		status.attach(to: decode)

		super.init(queue: queueName, childs: [network, status, decode])

		resend(input: network, select: { $0 as ConvertibleQuery })
		decode.resend(output: self)
	}
}
