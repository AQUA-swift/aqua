//
//  NetworkOperation.swift
//  Operations
//
//  Created by NShtain on 25/01/2019.
//

import Foundation
import Extensions

open class NetworkOperation: COperation, InputOperation, OutputOperation {
	public var input: Pending<ConvertibleQuery> = .pending
	public var output: Pending<OperationResult<ResponseData>> = .pending

	open override func execute() {
		guard case .ready(let query) = input
			else { fatalError() }

		let configuration = URLSessionConfiguration.ephemeral
		let queue = OperationQueue(name: "NetworkOperation_" + self.hash.toString())
		let session = URLSession(configuration: configuration,
								 delegate: nil,
								 delegateQueue: queue)
		let request = query.toRequest()

		if let method = request.httpMethod, let string = request.url?.absoluteString {
			print("[NetworkOperation] start request: \(method) \(string)")
		}
		let task = session.dataTask(with: request) { [weak self] (data, response, error) in
			guard
				let statusCode = response?.status,
				let data = data
				else {
					if let error = error {
						self?.finish(withResult: .failure(error))
					} else {
						let error = ApplicationError(message: "Response without error and statusCode/data.")
						self?.finish(withResult: .failure(error))
					}
					return
			}
			print("[NetworkOperation] response.statusCode: \(statusCode)")

			let result = ResponseData(data: data, statusCode: statusCode)
			self?.finish(withResult: .success(result))
		}
		task.resume()
	}
}
