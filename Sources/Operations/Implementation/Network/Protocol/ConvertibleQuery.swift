//
//  ConvertibleQuery.swift
//  Operations
//
//  Created by NShtain on 25/01/2019.
//

import Foundation

public protocol ConvertibleQuery {
	func toRequest() -> URLRequest
}
