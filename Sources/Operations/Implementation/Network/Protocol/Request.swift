//
//  Request.swift
//  Operations
//
//  Created by NShtain on 25/01/2019.
//

import Foundation

public protocol Request: ConvertibleQuery {
	associatedtype Out: Decodable

	var route: Route { get }
	var headers: [String: String]? { get }
	var data: RequestData { get }
}
