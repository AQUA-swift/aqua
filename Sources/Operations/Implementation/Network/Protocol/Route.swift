//
//  Route.swift
//  Operations
//
//  Created by NShtain on 25/01/2019.
//

import Foundation

public protocol Route {
	static var host: URL { get }
	static var apiPath: String? { get }
	var path: String { get }
	var method: RequestMethod { get }
}
