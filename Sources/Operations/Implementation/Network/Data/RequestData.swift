//
//  RequestData.swift
//  Operations
//
//  Created by NShtain on 25/01/2019.
//

import Foundation

public enum RequestData {
	case void
	case encodable(Encodable)
	case dictionary([String:Any])
}

public struct ResponseData {
	public let data: Data
	public let statusCode: Int

	public init(data: Data, statusCode: Int) {
		self.data = data
		self.statusCode = statusCode
	}
}
