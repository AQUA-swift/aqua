//
//  NetworkError.swift
//  Operations
//
//  Created by NShtain on 31/01/2019.
//

import Foundation

public class NetworkError: LocalizedError {
	public let statusCode: Int
	public let data: Data?

	public init(statusCode: Int, data: Data?) {
		self.statusCode = statusCode
		self.data = data
	}
}
