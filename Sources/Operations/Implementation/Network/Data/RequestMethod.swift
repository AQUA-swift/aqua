//
//  RequestMethod.swift
//  Operations
//
//  Created by NShtain on 25/01/2019.
//

import Foundation

public enum RequestMethod {
	case get, post, patch, put, delete

	var name: String {
		switch self {
		case .get:
			return "GET"
		case .post:
			return "POST"
		case .patch:
			return "PATCH"
		case .put:
			return "PUT"
		case .delete:
			return "DELETE"
		}
	}
}
