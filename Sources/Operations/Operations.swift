//
//  Operations.swift
//  Operations
//
//  Created by NShtain on 18/12/2018.
//

import Foundation
import Extensions

public struct Framework {
	static let name = "pro.aqua.operations"
}

public struct Queues {
	private init() {}

	/// Queue to resend data from OutputOperation to InputOperation
	static let adapterQueue = OperationQueue(name: Framework.name + ".adapterQueue")
	static var queues: [String: OperationQueue] = [:]
}

public extension Queues {
	static func queue(name: String) -> OperationQueue {
		if let queue = queues[name] {
			return queue
		} else {
			let queue = OperationQueue(name: name)
			queues[name] = queue
			return queue
		}
	}

	static func remove(queue name: String) {
		queues.removeValue(forKey: name)
	}
}
