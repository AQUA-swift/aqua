//
//  Pending.swift
//  Operations
//
//  Created by NShtain on 27/12/2018.
//

import Foundation

public enum Pending<T> {
	case pending
	case ready(T)

	public var isPending: Bool {
		guard case .pending = self else { return false }
		return true
	}

	public var value: T? {
		guard case let .ready(value) = self else { return nil }
		return value
	}

	public init(_ value: T?) {
		guard let value = value
			else {
				self = .pending
				return
		}
		self = .ready(value)
	}
}

extension Pending: Equatable where T: Equatable {
	public static func == (lhs: Pending<T>, rhs: Pending<T>) -> Bool {
		switch (lhs, rhs) {
		case (.pending, .pending):
			return true
		case let (.ready(lhsValue), .ready(rhsValue)):
			return lhsValue == rhsValue
		default:
			return false
		}
	}
}

public extension Optional {
	var pending: Pending<Wrapped> {
		switch self {
		case .none:
			return .pending
		case let .some(value):
			return .ready(value)
		}
	}
}
