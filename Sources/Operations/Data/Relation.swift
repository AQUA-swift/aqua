//
//  Relation.swift
//  Operations
//
//  Created by NShtain on 08/02/2019.
//

import Foundation

public enum Relation {
	case empty
	case cancel
	case cancelWhenFinish
}
