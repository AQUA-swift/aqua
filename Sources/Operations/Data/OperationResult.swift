//
//  OperationResult.swift
//  Operations
//
//  Created by NShtain on 27/12/2018.
//

import Foundation

public enum OperationResult<T> {
	case success(T)
	case failure(Error)
}

extension OperationResult: Equatable where T: Equatable {
	public static func == (lhs: OperationResult<T>, rhs: OperationResult<T>) -> Bool {
		switch (lhs, rhs) {
		case let (.success(lhsValue), .success(rhsValue)):
			return lhsValue == rhsValue
		default:
			return false
		}
	}
}
