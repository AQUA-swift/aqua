//
//  GroupOperation.swift
//  Operations
//
//  Created by NShtain on 27/12/2018.
//

import Foundation

open class CGroup: COperation {
	private let _queueName: String
	let queue: OperationQueue

	public var childs: [COperation]

	public init(queue: String, childs: [COperation]) {
		_queueName = queue
		self.queue = Queues.queue(name: _queueName)

		self.childs = childs
	}

	deinit {
		Queues.remove(queue: _queueName)
	}

	open func willFinished() {
		finish()
	}

	override open func execute() {
		for item in childs {
			item.addObserver(self, forKeyPath: "isFinished", options: .new, context: nil)
		}
		queue.addOperations(childs, waitUntilFinished: false)
	}

	override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		let key = keyPath!
		switch key {
		case "isFinished":
			let finished = !(childs.map { $0.isFinished }.contains(false))
			if finished && !self.isFinished {
				for item in childs {
					item.removeObserver(self, forKeyPath: "isFinished")
				}
				willFinished()
			}
		default: break
		}
	}
}
