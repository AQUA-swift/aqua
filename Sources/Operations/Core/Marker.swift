//
//  Marker.swift
//  Operations
//
//  Created by NShtain on 29/01/2019.
//

import Foundation

struct Marker {
	private init() {}
}

extension Marker {
	class Input<Source: InputOperation, Target: InputOperation>: COperation where Source.Input == Target.Input {
		let source: Source
		let target: Target
		required init(from source: Source, to target: Target) {
			self.source = source
			self.target = target
			super.init()
			ready()
		}

		override func execute() {
			if let operation = source as? Operation {
				operation.addObserver(self, forKeyPath: "isReady", options: .new, context: nil)
			}
		}

		override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
			let key = keyPath!
			switch key {
			case "isReady":
				if case .ready(let value) = source.input {
					target.set(input: value)
					if let operation = source as? Operation {
						operation.removeObserver(self, forKeyPath: "isReady")
					}
					finish()
				}
			default: break
			}
		}
	}
}

extension Marker {
	class InputWithSelect<Source: InputOperation, Target: InputOperation>: COperation {
		typealias Select = (Source.Input) -> Target.Input

		let source: Source
		let target: Target
		let select: Select
		required init(from source: Source, to target: Target, select: @escaping Select) {
			self.source = source
			self.target = target
			self.select = select
			super.init()
			ready()
		}

		override func execute() {
			if let operation = source as? Operation {
				operation.addObserver(self, forKeyPath: "isReady", options: .new, context: nil)
			}
		}

		override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
			let key = keyPath!
			switch key {
			case "isReady":
				if case .ready(let value) = source.input {
					target.set(input: select(value))
					if let operation = source as? Operation {
						operation.removeObserver(self, forKeyPath: "isReady")
					}
					finish()
				}
			default: break
			}
		}
	}
}

extension Marker {
	class Output<Source: OutputOperation, Target: OutputOperation>: COperation where Source.Output == Target.Output {
		let source: Source
		let target: Target
		required init(from source: Source, to target: Target) {
			self.source = source
			self.target = target
			super.init()
			ready()
		}

		override func execute() {
			defer { finish() }
			target.output = source.output
		}
	}

	class Error<Source: OutputOperation, Target: InputOperation>: COperation where Target.Input == Swift.Error {
		let source: Source
		let target: Target
		required init(from source: Source, to target: Target) {
			self.source = source
			self.target = target
			super.init()
			ready()
		}

		override func execute() {
			defer { finish() }
			if case .ready(let result) = source.output,
				case .failure(let error) = result {
				target.set(input: error)
			}
		}
	}

	class ErrorAsOutput<Source: OutputOperation, Target: OutputOperation>: COperation {
		let source: Source
		let target: Target
		required init(from source: Source, to target: Target) {
			self.source = source
			self.target = target
			super.init()
			ready()
		}

		override func execute() {
			defer { finish() }
			if case .ready(let result) = source.output,
				case .failure(let error) = result {
				target.output = .ready(.failure(error))
			}
		}
	}
}
