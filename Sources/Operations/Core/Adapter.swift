//
//  Adapter.swift
//  Operations
//
//  Created by NShtain on 28/12/2018.
//

import Foundation

struct Adapter {
	private init() {}
}

extension Adapter {
	class Result<Source: OutputOperation, Target: InputOperation>: COperation, AdapterResultProtocol where Source.Output == Target.Input {
		let source: Source
		let target: Target
		required init(from source: Source, to target: Target) {
			self.source = source
			self.target = target
			super.init()
			status = .ready
		}

		override func execute() {
			guard case .ready(let result) = source.output, !source.isCancelled
				else {
					cancel()
					target.cancel()
					return
			}
			switch result {
			case .success(let value):
				target.set(input: value)
			case .failure(let error):
				print(error.localizedDescription)
				target.cancel()
			}
			finish()
		}
	}
}

extension Adapter {
	class ResultWithSelect<Source: OutputOperation, Target: InputOperation>: COperation {
		typealias Select = (Source.Output) -> Target.Input

		let source: Source
		let target: Target
		let select: Select
		required init(from source: Source, to target: Target, select: @escaping Select) {
			self.source = source
			self.target = target
			self.select = select
			super.init()
			status = .ready
		}

		override func execute() {
			guard case .ready(let result) = source.output, !source.isCancelled
				else {
					cancel()
					target.cancel()
					return
			}
			switch result {
			case .success(let value):
				let result = select(value)
				target.set(input: result)
			case .failure(let error):
				print(error.localizedDescription)
				target.cancel()
			}
			finish()
		}
	}
}

extension Adapter {
	class Error<Source: OutputOperation, Target: InputOperation>: COperation, AdapterErrorProtocol where Target.Input == Swift.Error {
		typealias From = Source
		typealias To = Target

		let source: Source
		let target: Target
		required init(from source: Source, to target: Target) {
			self.source = source
			self.target = target
			super.init()
			status = .ready
		}

		override func execute() {
			guard case .ready(let result) = source.output, !source.isCancelled
				else {
					cancel()
					target.cancel()
					return
			}
			switch result {
			case .success:
				target.cancel()
			case .failure(let error):
				target.set(input: error)
			}
			finish()
		}
	}
}
