//
//  COperation.swift
//  Operations
//
//  Created by NShtain on 27/12/2018.
//

import Foundation
import Extensions

open class COperation: Operation, COperationProtocol {
	// MARK: Lyfecycle
	public override init() {
		let name = type(of: self).nameOfClass
		ifDebugPrint("[Lifecycle] init - \(name)")
		super.init()

		addObserver(self, forKeyPath: InnerStatus.ready.keyPath, options: .new, context: nil)
		addObserver(self, forKeyPath: InnerStatus.executing.keyPath, options: .new, context: nil)
		addObserver(self, forKeyPath: InnerStatus.cancelled.keyPath, options: .new, context: nil)
		addObserver(self, forKeyPath: InnerStatus.finished.keyPath, options: .new, context: nil)
	}

	deinit {
		let name = type(of: self).nameOfClass
		ifDebugPrint("[Lifecycle] deinit - \(name)")

		removeObserver(self, forKeyPath: InnerStatus.ready.keyPath)
		removeObserver(self, forKeyPath: InnerStatus.executing.keyPath)
		removeObserver(self, forKeyPath: InnerStatus.cancelled.keyPath)
		removeObserver(self, forKeyPath: InnerStatus.finished.keyPath)
	}

	private var relations: [(item: Weak<COperation>, relation: Relation)] = []

	// MARK: Statuses
	public var status: COperationStatus = .pending {
		didSet {
			innerStatus = status.toInnerStatus()
		}
	}

	private var innerStatus: InnerStatus = .pending {
		willSet {
			willChangeValue(forKey: innerStatus.keyPath)
			willChangeValue(forKey: newValue.keyPath)
		}
		didSet {
			didChangeValue(forKey: oldValue.keyPath)
			didChangeValue(forKey: innerStatus.keyPath)
		}
	}

	private var didCancel: Bool = false

	// MARK: Default Operation Variables
	open override var isReady: Bool {
		return super.isReady && innerStatus == .ready
	}
	open override var isFinished: Bool {
		return innerStatus == .finished
	}
	open override var isExecuting: Bool {
		return innerStatus == .executing
	}
	open override var isCancelled: Bool {
		return super.isCancelled && (didCancel || innerStatus == .cancelled)
	}

	// MARK: Default Operation Methods
	open override func start() {
		guard !isCancelled else { return }
		main()
	}

	open override func main() {
		guard !isCancelled else { return }
		status = .executing
		execute()
	}

	open override func cancel() {
		guard !isCancelled else { return }
		super.cancel()
		status = .cancelled
		didCancel = true
		finish()
	}

	// MARK: COperationProtocol Methods

	open func execute() {
		abstractMethod()
	}

	public func finish() {
		status = .finished

		for (item, relation) in relations {
			switch relation {
			case .cancel:
				if didCancel {
					let cancelled = item.value?.isCancelled ?? true
					if !cancelled {
						item.value?.cancel()
					}
				}
			case .cancelWhenFinish:
				if !didCancel {
					let cancelled = item.value?.isCancelled ?? true
					if !cancelled {
						item.value?.cancel()
					}
				}
			default: break
			}
		}
	}

	public func ready() {
		guard status == .pending else { return }
		status = .ready
	}

	public func add<Dependency>(dependency: Dependency) where Dependency: COperationProtocol {
		guard let operation = dependency as? Operation else { return }
		self.addDependency(operation)
	}

	public func add<Dependency>(dependencies: Dependency...) where Dependency : COperationProtocol {
		for item in dependencies {
			self.add(dependency: item)
		}
	}

	public func add<Item>(relation: Relation, for item: Item) where Item : COperationProtocol {
		guard
			let operation = item as? COperation,
			relation != .empty
			else { return }
		self.relations.append((Weak(operation), relation))
	}

	override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		let key = keyPath!
		let name = type(of: self).nameOfClass
		if change?[NSKeyValueChangeKey.newKey] as? Int == 1 {
			switch key {
			case InnerStatus.ready.keyPath:
				ifDebugPrint("[Opearation] \(name).status = \(key) ")
			case InnerStatus.executing.keyPath:
				ifDebugPrint("[Opearation] \(name).status = \(key) ")
			case InnerStatus.cancelled.keyPath:
				ifDebugPrint("[Opearation] \(name).status = \(key) ")
			case InnerStatus.finished.keyPath:
				ifDebugPrint("[Opearation] \(name).status = \(key) ")
			default:
				break
			}
		}
	}
}

// MARK: Statuses
extension COperation {
	fileprivate enum InnerStatus: String {
		case pending, ready, executing, finished, cancelled

		var keyPath: String {
			return "is" + rawValue.capitalized
		}
	}
}

extension COperationStatus {
	fileprivate func toInnerStatus() -> COperation.InnerStatus {
		switch self {
		case .pending:
			return .pending
		case .ready:
			return .ready
		case .executing:
			return .executing
		case .cancelled:
			return .cancelled
		case .finished, .failed:
			return .finished
		}
	}
}
