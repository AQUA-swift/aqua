//
//  InputOperation+Resend.swift
//  Operations
//
//  Created by NShtain on 11/02/2019.
//

import Foundation

public extension InputOperation {
	@discardableResult func resend<Next: InputOperation>(input operation: Next, relation: Relation = .empty) -> COperation where Input == Next.Input {
		let marker = Marker.Input(from: self, to: operation)

		self.add(relation: relation, for: marker)
		marker.add(relation: .cancel, for: operation)

		Queues.adapterQueue.addOperation(marker)

		return marker
	}

	@discardableResult func resend<Next: InputOperation>(input operation: Next, relation: Relation = .empty, select: @escaping (Input) -> Next.Input) -> COperation {
		let marker = Marker.InputWithSelect(from: self, to: operation, select: select)

		self.add(relation: relation, for: marker)
		marker.add(relation: .cancel, for: operation)

		Queues.adapterQueue.addOperation(marker)

		return marker
	}
}
