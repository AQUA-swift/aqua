//
//  OutputOperation+Resend.swift
//  Operations
//
//  Created by NShtain on 11/02/2019.
//

import Foundation

public extension OutputOperation {
	@discardableResult func resend<Next: OutputOperation>(output operation: Next, relation: Relation = .empty) -> COperation where Output == Next.Output {
		let marker = Marker.Output(from: self, to: operation)
		marker.add(dependency: self)

		self.add(relation: relation, for: marker)
		marker.add(relation: .cancel, for: operation)

		Queues.adapterQueue.addOperation(marker)

		return marker
	}

	@discardableResult func resend<Next: InputOperation>(errorToInput operation: Next, relation: Relation = .empty) -> COperation where Next.Input == Swift.Error {
		let marker = Marker.Error(from: self, to: operation)
		marker.add(dependency: self)

		self.add(relation: relation, for: marker)
		marker.add(relation: .cancel, for: operation)

		Queues.adapterQueue.addOperation(marker)

		return marker
	}

	@discardableResult func resend<Next: OutputOperation>(error operation: Next, relation: Relation = .empty) -> COperation {
		let marker = Marker.ErrorAsOutput(from: self, to: operation)
		marker.add(dependency: self)

		self.add(relation: relation, for: marker)
		marker.add(relation: .cancel, for: operation)

		Queues.adapterQueue.addOperation(marker)

		return marker
	}
}
