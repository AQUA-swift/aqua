//
//  OutputOperation+Attach.swift
//  Operations
//
//  Created by NShtain on 11/02/2019.
//

import Foundation

public extension OutputOperation {
	/// Resend data when output == .success
	///
	/// Operation *to* will be canceled if output == .failure
	func attach<Next: InputOperation>(to operation: Next, relation: Relation = .cancel) where Next.Input == Output {
		let adapter = Adapter.Result(from: self, to: operation)

		adapter.add(dependency: self)
		operation.add(dependency: adapter)

		self.add(relation: relation, for: adapter)
		adapter.add(relation: .cancel, for: operation)

		Queues.adapterQueue.addOperation(adapter)
	}

	/// Resend error when output == .failure
	///
	/// Operation *to* will be canceled if output == .success
	func attach<Next: InputOperation>(to operation: Next, relation: Relation = .cancel) where Next.Input == Swift.Error {
		let adapter = Adapter.Error(from: self, to: operation)

		adapter.add(dependency: self)
		operation.add(dependency: adapter)

		self.add(relation: relation, for: adapter)
		adapter.add(relation: .cancel, for: operation)

		Queues.adapterQueue.addOperation(adapter)
	}

	func attach<Next: InputOperation>(to operation: Next, relation: Relation = .cancel, select: @escaping (Output) -> Next.Input) {
		let adapter = Adapter.ResultWithSelect(from: self, to: operation, select: select)

		adapter.add(dependency: self)
		operation.add(dependency: adapter)

		self.add(relation: relation, for: adapter)
		adapter.add(relation: .cancel, for: operation)

		Queues.adapterQueue.addOperation(adapter)
	}
}
