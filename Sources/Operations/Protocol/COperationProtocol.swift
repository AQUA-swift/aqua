//
//  COperationProtocol.swift
//  Operations
//
//  Created by NShtain on 27/12/2018.
//

import Foundation

public protocol COperationProtocol: class {
	var status: COperationStatus { get }

	var isReady: Bool { get }
	var isExecuting: Bool { get }
	var isFinished: Bool { get }
	var isCancelled: Bool { get }

	func ready()
	func execute()
	func finish()
	func cancel()

	func add<Dependency: COperationProtocol>(dependency: Dependency)
	func add<Dependency: COperationProtocol>(dependencies: Dependency...)
	func add<Item: COperationProtocol>(relation: Relation, for item: Item)
}

public enum COperationStatus {
	case pending, ready, executing, cancelled, failed, finished
}
