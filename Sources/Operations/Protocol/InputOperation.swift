//
//  InputOperation.swift
//  Operations
//
//  Created by NShtain on 27/12/2018.
//

import Foundation

public protocol InputOperation: COperationProtocol {
	associatedtype Input
	var input: Pending<Input> { get set }
}

public extension InputOperation {
	func set(input: Input) {
		self.input = .ready(input)
		self.ready()
	}
}
