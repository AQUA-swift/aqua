//
//  AdapterErrorProtocol.swift
//  Operations
//
//  Created by NShtain on 27/12/2018.
//

import Foundation

protocol AdapterErrorProtocol: COperationProtocol {
	associatedtype From: OutputOperation
	associatedtype To: InputOperation where To.Input == Error

	var source: From { get }
	var target: To { get }
	init(from source: From, to target: To)
}
