//
//  OutputOperation.swift
//  Operations
//
//  Created by NShtain on 27/12/2018.
//

import Foundation

public protocol OutputOperation: COperationProtocol {
	associatedtype Output
	var output: Pending<OperationResult<Output>> { get set }
}

public extension OutputOperation {
	func finish(withResult result: OperationResult<Output>) {
		output = .ready(result)
		finish()
	}
}
