//
//  UIViewLoading.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

#if canImport(UIKit)
import Foundation
import UIKit

public protocol UIViewLoading: class {}
extension UIView : UIViewLoading {}

public extension UIViewLoading where Self == UIView {
	static func loadFromNib() -> Self {
		return instantiateViewFromNib()
	}

	private static func instantiateViewFromNib<T: UIView>() -> T {
		return UINib(nibName: nameOfClass, bundle: nil).instantiate(withOwner: nil, options: nil).first as! T
	}
}
#endif
