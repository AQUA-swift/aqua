//
//  URLResponse.swift
//  Operations
//
//  Created by NShtain on 25/01/2019.
//

import Foundation

public extension URLResponse {
	var status: Int? {
		return (self as? HTTPURLResponse)?.statusCode
	}
}
