//
//  Enum.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public protocol EnumCollection: Hashable {
	static func cases() -> AnySequence<Self>
	static var allValues: [Self] { get }
}

public extension EnumCollection {
	static func cases() -> AnySequence<Self> {
		return AnySequence { () -> AnyIterator<Self> in
			var raw = 0
			return AnyIterator {
				let current: Self = withUnsafePointer(to: &raw) {
					$0.withMemoryRebound(to: self, capacity: 1) { $0.pointee }
				}
				guard current.hashValue == raw else {
					return nil
				}
				raw += 1
				return current
			}
		}
	}

	static var allValues: [Self] {
		return Array(self.cases())
	}
}
