//
//  Int.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public extension Int {
	static func random(before: Int) -> Int {
		let randomNum: Int = Int(arc4random_uniform(UInt32(before)))
		return randomNum
	}

	static func random(from: Int, to: Int) -> Int {
		return random(before: to - from + 1) + from
	}

	func toString() -> String {
		return "\(self)"
	}

	func toDouble() -> Double {
		return Double(self)
	}

	var even: Bool {
		return self % 2 == 0
	}
}
