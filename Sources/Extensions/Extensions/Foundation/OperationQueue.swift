//
//  OperationQueue.swift
//  Extensions
//
//  Created by NShtain on 27/12/2018.
//

import Foundation

extension OperationQueue {
	public convenience init(name: String) {
		self.init()
		self.name = name
	}
}
