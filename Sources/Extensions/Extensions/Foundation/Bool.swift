//
//  Bool.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public extension Bool {
	@discardableResult mutating func toggle() -> Bool {
		self = !self
		return self
	}
}
