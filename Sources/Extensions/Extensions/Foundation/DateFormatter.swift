//
//  DateFormatter.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public extension DateFormatter {
	static var current: DateFormatter {
		let formatter = DateFormatter()
		formatter.calendar = Calendar.current
		formatter.locale = Locale.current
		formatter.timeZone = TimeZone.current
		return formatter
	}
}
