//
//  Date.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public extension Date {
	var components: DateComponents {
		return Calendar.current.dateComponents(
			[.month, .year, .hour, .minute, .second, .day],
			from: self)
	}

	var year: Int { return self.components.year! }
	var month: Int { return self.components.month! }
	var day: Int { return self.components.day! }
	var hour: Int { return self.components.hour! }
	var minute: Int { return self.components.minute! }

	var age: Int {
		return Calendar.current.dateComponents([.year], from: self, to: Date()).year!
	}

	func add(seconds: Int) -> Date? {
		var dayComponent: DateComponents = DateComponents()
		dayComponent.second = seconds
		return NSCalendar.current.date(
			byAdding: dayComponent,
			to: self)
	}

	func add(minute: Int) -> Date? {
		var dayComponent: DateComponents = DateComponents()
		dayComponent.minute = minute
		return NSCalendar.current.date(
			byAdding: dayComponent,
			to: self)
	}

	func add(hour: Int) -> Date? {
		var dayComponent: DateComponents = DateComponents()
		dayComponent.hour = hour
		return NSCalendar.current.date(
			byAdding: dayComponent,
			to: self)
	}

	func add(days: Int) -> Date? {
		var dayComponent: DateComponents = DateComponents()
		dayComponent.day = days
		return NSCalendar.current.date(
			byAdding: dayComponent,
			to: self)
	}

	func add(year: Int) -> Date? {
		var yearComponent: DateComponents = DateComponents()
		yearComponent.year = year
		return NSCalendar.current.date(
			byAdding: yearComponent,
			to: self)
	}

	func toString(_ format: DateFormats) -> String {
		let formatter = DateFormatter.current
		formatter.dateFormat = format.rawValue
		return formatter.string(from: self)
	}

	init(year: Int? = nil, month: Int? = nil, day: Int? = nil, hour: Int? = nil, minute: Int? = nil) {
		self.init()
		var component = DateComponents()
		let current = Date()
		component.year = year ?? current.year
		component.month = month ?? current.month
		component.day = day ?? current.day
		component.hour = hour ?? current.hour
		component.minute = minute ?? current.minute

		self = Calendar.current.date(from: component)!
	}
}
