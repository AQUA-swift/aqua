//
//  Array.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public extension Array {
	func random() -> Element {
		return self[Int.random(before: self.count)]
	}

	func cycle(index: Int) -> Element {
		guard count > index else {
			return cycle(index: index - count)
		}
		return self[index]
	}
}
