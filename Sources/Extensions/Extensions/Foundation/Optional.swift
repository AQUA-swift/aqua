//
//  Optional.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public extension Optional {
	func toArray() -> [Wrapped] {
		switch self {
		case .none:
			return []
		case .some(let value):
			return [value]
		}
	}
}
