//
//  Codable.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public extension Encodable {
	func toDictionary() -> [String: Any]? {
		guard let data = toJsonData()
			else { return nil }
		return (try? JSONSerialization
			.jsonObject(with: data,
							options: .allowFragments)
			)
			.flatMap { $0 as? [String: Any] }
	}

	func toJsonData() -> Data? {
		return try? JSONEncoder().encode(self)
	}

	func save(key: String) {
		if let data = toJsonData() {
			UserDefaults.standard.set(data, forKey: key)
		}
	}
}

public extension Decodable {
	static func fromDictionary(_ dictionary: [String: Any]) -> Self? {
		guard
			let data = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
			else { return nil }
		return fromJsonData(data)
	}

	static func fromJsonData(_ data: Data) -> Self? {
		return try? JSONDecoder().decode(self, from: data)
	}

	static func load(key: String) -> Self? {
		if let data = UserDefaults.standard.data(forKey: key) {
			return fromJsonData(data)
		}
		return nil
	}

	static func delete(key: String) {
		if UserDefaults.standard.data(forKey: key) != nil {
			UserDefaults.standard.removeObject(forKey: key)
		}
	}
}
