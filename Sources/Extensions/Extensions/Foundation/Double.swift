//
//  Double.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public extension Double {
	// fixme: create custom type
	func toSum(locale: Locale = Locale(identifier: "ru_RU")) -> String {
		let formatter = NumberFormatter()
		formatter.locale = locale
		formatter.numberStyle = .currency
		if let formattedTipAmount = formatter.string(from: NSNumber(value: self)) {
			return formattedTipAmount
		}
		return (locale.currencySymbol ?? "") + "\(self)"
	}

	func toString(decimalPlaces count: Int) -> String {
		let formatter = NumberFormatter()
		formatter.numberStyle = .decimal
		formatter.minimumFractionDigits = count
		formatter.maximumFractionDigits = count
		return formatter.string(from: NSNumber(value: self))!
	}

	func toString() -> String {
		return "\(self)"
	}
}
