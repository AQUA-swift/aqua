//
//  String.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public extension String {
	func toDate(_ format: DateFormats) -> Date? {
		let formatter = DateFormatter.current
		formatter.dateFormat = format.rawValue
		return formatter.date(from: self)
	}

	func localise(comment:String? = "") -> String {
		return NSLocalizedString(self, comment: "")
	}

	var nilOnEmpty: String? { return self.isEmpty ? nil : self }

	func toInt() -> Int? {
		return Int(self)
	}

	func toDouble() -> Double? {
		return Double(self)
	}

	func escaped(with characterSet: CharacterSet = .urlQueryAllowed) -> String {
		return self.addingPercentEncoding(withAllowedCharacters: characterSet) ?? self
	}
}

// fixme: change names
public extension Optional where Wrapped == String {
	var emptyOnNil: String {
		switch self {
		case .some(let value):
			let res = String(value)
			return res
		default:
			return ""
		}
	}

	func ifNotNilAdd(before: String = "", after: String) -> String {
		switch self {
		case .none:
			return ""
		case .some(let value):
			return before + value + after
		}
	}

	func doIfNotNil<T>(_ handler: (String) -> T) -> T? {
		switch self {
		case .some(let value):
			return handler(value)
		case .none:
			return nil
		}
	}
}
