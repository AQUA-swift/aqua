//
//  Data.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public extension Data {
	var imageContentType: String {
		var contentTypeCode = [UInt8](repeating: 0, count: 1)
		self.copyBytes(to: &contentTypeCode, count: 1)

		switch (contentTypeCode[0]) {
		case 0xFF:
			return "image/jpeg"
		case 0x89:
			return "image/png"
		case 0x47:
			return "image/gif"
		case 0x49:
			return "image/tiff"
		case 0x4D:
			return "image/tiff"
		default:
			return ""
		}
	}

	func toString(encoding: String.Encoding = .utf8) -> String? {
		return String(data: self, encoding: encoding)
	}

	func decode<T: Decodable>(of type: T.Type) -> T? {
		return try? JSONDecoder().decode(type, from: self)
	}

	var hexString: String {
		return map { String(format: "%02.2hhx", $0) }.joined()
	}
}
