//
//  NSObject.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public extension NSObject {
	static var nameOfClass: String {
		return String(describing: self)
	}

	static var fullNameOfClass: String {
		return NSStringFromClass(self)
	}
}
