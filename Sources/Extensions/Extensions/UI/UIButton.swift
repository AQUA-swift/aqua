//
//  UIButton.swift
//  AQExtensions
//
//  Created by nshtain on 14/11/2018.
//

#if canImport(UIKit)
import UIKit

public extension UIButton {
	public func disable() {
		isEnabled = false
	}

	public func enable() {
		isEnabled = true
	}
}
#endif
