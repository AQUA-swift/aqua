//
//  UIApplication.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

#if canImport(UIKit)
import UIKit

public extension UIApplication {
	static func topViewController(base: UIViewController? = UIApplication.shared.delegate?.window??.rootViewController) -> UIViewController? {
		if let nav = base as? UINavigationController {
			return topViewController(base: nav.visibleViewController)
		}

		if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
			return topViewController(base: selected)
		}

		if let presented = base?.presentedViewController {
			return topViewController(base: presented)
		}

		return base
	}

	static func baseViewController() throws -> UIViewController {
		guard
			let viewController = UIApplication.shared.delegate?.window??.rootViewController ??
				UIApplication.shared.keyWindow?.rootViewController
			else { fatalError("NO VISIBLE VIEW CONTROLLER") }
		return viewController
	}
}
#endif
