//
//  UITextField.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

#if canImport(UIKit)
import UIKit

public extension UITextField {
	func clear() {
		self.text = nil
	}
}
#endif
