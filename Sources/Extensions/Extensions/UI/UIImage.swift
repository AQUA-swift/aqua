//
//  UIImage.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

#if canImport(UIKit)
import UIKit

public extension UIImage {
	convenience init?(color: UIColor = UIColor.clear, size: CGSize = CGSize(width: 0.5, height: 0.5)) {
		let rect = CGRect(origin: .zero, size: size)
		UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
		color.setFill()
		UIRectFill(rect)
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		guard let cgImage = image?.cgImage else {
			return nil
		}
		self.init(cgImage: cgImage)
	}

	convenience init?(color: UIColor, capInsets: UIEdgeInsets) {
		let rect = CGRect(origin: CGPoint(x: 0, y:0), size: CGSize(width: 1, height: 1))
		UIGraphicsBeginImageContext(rect.size)

		guard let context = UIGraphicsGetCurrentContext() else {
			return nil
		}

		context.setFillColor(color.cgColor)
		context.fill(rect)

		guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
			UIGraphicsEndImageContext()
			return nil
		}

		guard let cgImage = image.resizableImage(withCapInsets: capInsets).cgImage else {
			return nil
		}
		self.init(cgImage: cgImage)
	}

	static var clear: UIImage {
		return UIImage(color: .clear)!
	}

	func resize(max: CGFloat) -> UIImage {
		let currenetMax = CGFloat.maximum(self.size.width, self.size.height)
		if currenetMax <= max {
			return self
		}
		let delta = currenetMax / max
		let size = CGSize(width: self.size.width / delta, height: self.size.height / delta)

		let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)

		UIGraphicsBeginImageContextWithOptions(size, false, 1.0)
		self.draw(in: rect)
		let newImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()

		return newImage!
	}
}
#endif
