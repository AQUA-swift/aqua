//
//  UIViewController.swift
//  AQExtensions
//
//  Created by nshtain on 15/11/2018.
//

#if canImport(UIKit)
import UIKit

public extension UIViewController {
	public func wrapNavigation() -> UINavigationController {
		return UINavigationController(rootViewController: self)
	}
}
#endif
