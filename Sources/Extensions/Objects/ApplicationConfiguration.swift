//
//  ApplicationConfiguration.swift
//  Extensions
//
//  Created by NShtain on 30/01/2019.
//

import Foundation

public enum ApplicationConfiguration {
	case debug
	case testing
	case production
}

public extension ApplicationConfiguration {
	private static var isTestFlight: Bool {
		return Bundle.main.appStoreReceiptURL?.lastPathComponent == "sandboxReceipt"
	}
	private static var isDebug: Bool {
		#if DEBUG
		return true
		#else
		return false
		#endif
	}

	public static var current: ApplicationConfiguration {
		if isDebug {
			return .debug
		} else if isTestFlight {
			return .testing
		} else {
			return .production
		}
	}
}
