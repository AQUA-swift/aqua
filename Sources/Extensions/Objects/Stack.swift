//
//  Stack.swift
//  AQExtensions
//
//  Created by shtain_n on 05.10.2018.
//

import Foundation

public class Stack<T> {
	private var items: [T] = []

	public init(value: T? = nil) {
		if let value = value {
			self.push(value)
		}
	}

	public func push(_ value: T) {
		items.append(value)
	}

	public func pop() -> T? {
		return items.popLast()
	}

	public func peek() -> T? {
		return items.last
	}

	public var count: Int {
		return items.count
	}
}
