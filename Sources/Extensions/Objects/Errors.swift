//
//  Errors.swift
//  AQExtensions
//
//  Created by shtain_n on 05.11.2018.
//

import Foundation

open class ApplicationError: LocalizedError {
    public let message: String
    public init(message: String? = nil) {
        self.message = message ?? "unknown"
    }

    open var errorDescription: String? {
		return NSLocalizedString(message, comment: "")
	}
}

public final class ValidationError: ApplicationError {
	public let values: [String:[String]]
	public init(message: String?, values: [String:[String]]) {
        self.values = values
		let message = message ?? "Not right data"
        super.init(message: message)
    }

	public override var errorDescription: String? {
		return "НЕ ПОКАЗЫВАТЬ ОШИБКУ ВАЛИДАЦИИ!"
	}
}
