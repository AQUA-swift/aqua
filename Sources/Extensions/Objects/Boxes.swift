//
//  Boxes.swift
//  AQExtensions
//
//  Created by shtain_n on 14.08.2018.
//  Copyright © 2018 shtain_n. All rights reserved.
//

import Foundation

public protocol UnownedBox {
	associatedtype T: AnyObject
	var value: T { get }
}

public protocol WeakBox {
	associatedtype T: AnyObject
	var value: T? { get }
}

public struct Unowned<T: AnyObject>: UnownedBox {
	public private(set) unowned var value: T

	public init(_ object: T) {
		self.value = object
	}
}

public struct Weak<T: AnyObject>: WeakBox {
	public private(set) weak var value: T?

	public init(_ object: T) {
		self.value = object
	}
}
