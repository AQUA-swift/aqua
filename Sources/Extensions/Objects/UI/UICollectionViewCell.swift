//
//  UICollectionViewCell.swift
//  AQExtensions
//
//  Created by nshtain on 14/11/2018.
//

#if canImport(UIKit)
import UIKit

open class AutolayoutCollectionViewCell: UICollectionViewCell {
	open var direction: Direction { return [.width, .height] }

	open override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
		let size = contentView.sizeThatFits(layoutAttributes.size)
		var newFrame = layoutAttributes.frame
		if direction.contains(.height) {
			newFrame.size.height = CGFloat(ceilf(Float(size.height)))
		}
		if direction.contains(.width) {
			newFrame.size.width = CGFloat(ceilf(Float(size.width)))
		}
		layoutAttributes.frame = newFrame

		return layoutAttributes
	}

	public struct Direction: OptionSet {
		public let rawValue: Int
		public init(rawValue: Int) {
			self.rawValue = rawValue
		}

		public static let width = Direction(rawValue: 1 << 0)
		public static let height = Direction(rawValue: 1 << 1)
	}
}
#endif
