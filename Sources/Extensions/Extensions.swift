//
//  Extensions.swift
//  Extensions
//
//  Created by NShtain on 19/12/2018.
//

import Foundation

public enum DateFormats: String {
	/// "23:59"
	case time = "hh:mm"

	/// "31.12.1900"
	case numbers = "dd.MM.y"

	/// "31.12.1900 23:59"
	case datetime = "dd.MM.y HH:mm"

	/// "31.12.1900 23:59:59"
	case full = "dd.MM.y HH:mm:ss"

	/// "2018-04-25T11:06:36"
	case server = "y-MM-dd'T'HH:mm:ss"

	/// "2018-09-05T11:42:20.476320Z"
	case serverFull = "yyyy-MM-dd'T'HH:mm:ss.ZZZZZ'Z'"

	/// "31 december 1900"
	case readableDate = "d MMM"

	/// "31 december 1900"
	case readable = "dd MMMM y"

	/// "Monday, 31 december 1900 23:59"
	case readableWithTime = "EEEE',' dd MMMM y HH:mm"

	/// "2000"
	case year = "y"

	/// "2000-12-31"
	case reverseNumbers = "y-MM-dd"
}

public func ifDebugPrint(_ message: String) {
	#if DEBUG
	print("[DEBUG] " + message)
	#endif
}

public func abstractMethod() -> Never {
	fatalError("Method must be override")
}
